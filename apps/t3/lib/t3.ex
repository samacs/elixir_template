defmodule T3 do
  @moduledoc """
  Documentation for T3.
  """

  @doc """
  Hello world.

  ## Examples

      iex> T3.hello()
      :world

  """
  def hello do
    :world
  end
end
