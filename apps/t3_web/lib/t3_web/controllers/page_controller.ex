defmodule T3.Web.PageController do
  use T3.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
