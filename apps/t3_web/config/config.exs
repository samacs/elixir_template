# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :t3_web,
  namespace: T3

# Configures the endpoint
config :t3_web, T3.Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "RFrSzSzfGKlOGFizNTg6CF1Ns466ASEmayQrogYkd2KAsM813oD2FVMsK9cQXD37",
  render_errors: [view: T3.Web.ErrorView, accepts: ~w(html json)],
  pubsub: [name: T3.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :t3_web, :generators,
  context_app: :t3

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
