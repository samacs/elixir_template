import { createBrowserHistory } from 'history'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import promiseMiddleware from 'redux-promise'
import { createLogger } from 'redux-logger'

export const history = createBrowserHistory()

import reducer from '../reducer'

const initialState = {}

const loggerMiddleware = createLogger()

export default createStore(
    reducer,
    initialState,
    applyMiddleware(
        thunkMiddleware,
        loggerMiddleware,
        promiseMiddleware
    )
)
