import { Record } from 'immutable'

const User = Record({
    name: '',
    email: '',
    password: '',
    passwordConfirmation: ''
})

const InitialState = Record({
    user: new User()
})

export default InitialState
