import {
    TOKEN_REQUEST,
    TOKEN_SUCCESS,
    TOKEN_FAILURE
} from './constants'
import InitialState from './state'

const initialState = new InitialState()

export default (state = initialState, action) => {
    if (!(state instanceof InitialState)) return initialState.mergeDeep(state)

    switch (action.type) {
        case TOKEN_REQUEST:
            return state
        case TOKEN_FAILURE:
            return state
        case TOKEN_SUCCESS:
            return state
        default:
            return state
    }
}
