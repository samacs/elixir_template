import {
    TOKEN_REQUEST,
    TOKEN_SUCCESS,
    TOKEN_FAILURE
} from './constants'

export const tokenRequest = () => ({
    type: TOKEN_REQUEST
})

export const tokenSuccess = (token) => ({
    type: TOKEN_SUCCESS,
    payload: token
})

export const tokenFailure = (error) => ({
    type: TOKEN_FAILURE,
    payload: error
})
