import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

const mapStateToProps = state => ({
    user: state.auth.user
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({}, dispatch)
})

class App extends Component {
    render () {
        const { user } = this.props
        return (
            <h3>Hi</h3>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
