const path = require('path')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
    entry: [
        '@babel/polyfill',
        './src/app.js',
        './src/scss/app.scss'
    ],
    devtool: process.env.NODE_ENV === 'production' ? 'source-map' : 'eval',
    output: {
        path: path.resolve(__dirname, '../priv/static'),
        filename: 'js/app.js',
        publicPath: '/'
    },
    module: {
        rules: [
            { test: /\.js$/, use: ['babel-loader'], include: path.join(__dirname, 'src') },
            { test: /\.(css|scss|sass)$/, use: ['style-loader', 'css-loader', 'sass-loader'] }
        ]
    },
    watch: true,
    plugins: [
        new CopyWebpackPlugin([
            { from: './static' }
        ]),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            popper: 'popper.js'
        })
    ],
    node: {
        fs: 'empty',
        net: 'empty',
        tls: 'empty'
    },
    resolve: {
        modules: ['node_modules', path.resolve(__dirname, 'src')],
        extensions: ['.js']
    }
}
